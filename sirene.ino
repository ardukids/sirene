int pinSirene = 8;

void setup() 
{
  pinMode(pinSirene, OUTPUT); 
}

void loop() 
{
  for (int i = 220; i <= 1760; i++)
  {
    tone(pinSirene, i);
    delay(5);  
  }

  for (int i = 1760; i >= 220; i--)
  {
    tone(pinSirene, i);
    delay(5);  
  }
}
